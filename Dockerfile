FROM openjdk:12
ADD target/docker-spring-boot-guilherme.jar docker-spring-boot-guilherme.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-guilherme.jar"]

