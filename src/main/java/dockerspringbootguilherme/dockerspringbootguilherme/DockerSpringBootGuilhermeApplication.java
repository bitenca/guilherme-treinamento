package dockerspringbootguilherme.dockerspringbootguilherme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerSpringBootGuilhermeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerSpringBootGuilhermeApplication.class, args);
	}

}
