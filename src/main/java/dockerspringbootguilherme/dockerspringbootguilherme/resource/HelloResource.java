package dockerspringbootguilherme.dockerspringbootguilherme.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/docker/hello")
public class HelloResource {

    @GetMapping
    public String hello() {
        return "Docker e Spring boot funcionando !!! na porta 8085 editado todo com o VIM, build com maven, artifactory e openshift V3";
    }
}
